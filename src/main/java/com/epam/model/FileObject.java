package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class FileObject implements Logable {

    private static Logger logger = LogManager.getLogger(FileObject.class);

    public void log() {
        logger.info("Info console log");
        logger.warn("Warn file log");
    }

}
