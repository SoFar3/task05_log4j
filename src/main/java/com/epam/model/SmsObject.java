package com.epam.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class SmsObject implements Logable {

    private static Logger logger = LogManager.getLogger(SmsObject.class);

    public void log() {
        logger.fatal("Fatal error during homework process");
    }

}
