package com.epam;

import com.epam.model.EmailObject;
import com.epam.model.FileObject;
import com.epam.model.Logable;
import com.epam.model.SmsObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;

public class App {

    private static Logger logger = LogManager.getLogger(App.class);

    private static ArrayList<Logable> logables = new ArrayList<Logable>();

    public static void main(String[] args) {
        logables.add(new SmsObject());
        logables.add(new EmailObject());
        logables.add(new FileObject());

        for (Logable logable : logables) {
            logable.log();
        }

        logger.error("The end.");
        logger.fatal("Another the end.");
    }

}
